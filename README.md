# Examen-2

Examen2

# Descripción

Este proyecto está diseñado para poner a prueba tu conocimiento en tecnologías web back-end, usando SpringBoot y así evaluar tu capacidad para crear productos back-end con atención a los detalles, estándares y reutilización. Debe ser un proyecto estándar de Maven; Se permite utilizar cualquier IDE de desarrollo. 
# Asignación

Se tiene una Tienda de teléfonos móviles la cual desea generar su propio sistema administrativo. Actualmente todo se hace de forma manual. Los teléfonos celulares que venden están registrados en un Excel donde se especifican la marca, el modelo, precio y código (este código es propio de la empresa). Por otro lado, al realizar una venta se registra al cliente, guardando la información de este (Nombre, Apellido, DNI, Dirección, teléfono) y finalmente se genera la orden de compra donde se registra el o los productos comprados, en esta orden contiene la información del cliente, la lista de productos comprados con su información, la fecha de la orden de compra, numero de orden de compra y el total a pagar. 
# Funciones Obligatorias 

3.1 Uso de Framework SpringBoot 3.2 Servicio que me cree una orden de compra. 
3.3 Generar un servicio que me retorne la lista de órdenes de compra. 
3.4 Generar un servicio que me retorne una orden de compra usando su número de orden. 
# Opcional
• Uso de Streams y Lambdas 
• Generar el resto de los servicios CRUD para Productos y clientes. 
• Test unitarios. 

# Pautas de Entrega
Al finalizar la tarea, comprima la misma en un archivo zip (No olvide incluir la carpeta. git) o suba el proyecto a un repositorio Git y comparta el link de acceso en el correo de respuesta. Si no pudiste terminar todo, deja indicado que partes del proyecto no pudiste completar. 

Resolución de examen en Spring Boot
Examen terminado:
Subido el proyecto a una imagen docker  ignaciomaylin/examen2:1.0
Para correr el proyecto tenes dos opciones:
1- descargandote el proyecto del repositorio de git lav y correrlo con una variable de entorno de java11 o superior
2- (si tenes docker) solo te descargas el archivo de docker-compose lo corres y listo.
1)como hago la opcion uno(necesitas variable de entorno java 11 a mas).
Asi tambien, necesitas tener mySql corriendo en el puerto 3600
con una base de datos llamada "examen2" porque esta configurado asi el propertis
-clonas el proyecto completo.
-lo abris en tu idea
-lo levantas o corres los test// los test de reposity y service necesitan que saques el cascade y una vez levantado 
 el ambiente comitiar el inicio que crea el excel y lo pasa a mysql cambiando el propertis a update
-si lo levantas podes importar el archivo examen2.postman_collection.json en tu
postman y tenes para probar los entry points (asegurate de tener el postmanAgent
levantado para poder hacer solicitudes locales, Asi tambien el puerto 8082 libre
para levantar el proyecto)
2)Como hago la opcion dos (suponiendo que tenes docker en tu maquina)
-descargate el solo archivo docker-compose.yam de la carpeta principal.
-desde la terminal donde se encuentra ese archivo que descargaste deveras correr los siguientes comandos
     :docker-compose up -d
     // una vez que te los descargo las imagenes y las corrio en contenedores
     //comprovar que esten levantados
     :docker ps 
     // si solo se levanto uno volve a correr ":docker-compose up -d"
     // una ves que esten los dos levantados podes probar a travez de postman
     // Descargate el archivo examen.postman_collection.json de la carpeta principal
     // Abri tu postman y importalo 
     // antes de probar comprova que los 2 contenedores esten corriendo
     // se conectan a travez de tu puerto 8082 por lo cual debe de estar libre
     // asegurate de tener el postmanAgent abierto para hacer solicitudes locales
     // desde el postman de tu navegador