package com.examen2.demo.component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.examen2.demo.model.celulares.G6;
import com.examen2.demo.model.celulares.G7;
import com.examen2.demo.model.celulares.G8;
import com.examen2.demo.model.celulares.GA;
import com.examen2.demo.model.celulares.GS;
import com.examen2.demo.model.celulares.Iphone11;
import com.examen2.demo.model.celulares.Iphone8;
import com.examen2.demo.model.entity.Celular;
import com.examen2.demo.model.entity.Cliente;
import com.examen2.demo.model.entity.DetallesDeOrdenDeCompra;
import com.examen2.demo.model.entity.OrdenDeCompra;
import com.examen2.demo.repository.CelularRepository;
import com.examen2.demo.repository.ClienteRepository;
import com.examen2.demo.repository.DetallesDeOrdenDeCompraRepository;
import com.examen2.demo.repository.OrdenDeCompraRepository;

@Component
public class DataLoader implements ApplicationRunner {

	@Autowired
	private CelularRepository celularRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private DetallesDeOrdenDeCompraRepository detallesDeOrdenDeCompraRepository;

	@Autowired
	private OrdenDeCompraRepository ordenDeCompraRepository;

	@Autowired
	private RandomService randomService;

	@Override
	public void run(ApplicationArguments args) throws Exception {

//Crear un excel con datos random como si fuera una empresa real que usaba excel para sus registros
		int cantidadCelulares = 10000;
		int cantidadClientes = 2000;
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheetCelulares = workbook.createSheet("Celulares");
		Set<Celular> celulares = cargarCelulares(cantidadCelulares);
		XSSFSheet sheetClientes = workbook.createSheet("Clientes");
		Set<Cliente> clientes = cargarClientes(cantidadClientes);
		XSSFSheet sheetOrdenesDeCompra = workbook.createSheet("OrdenesDeCompra");
		XSSFSheet sheetDetallesDeOrdenesDeCompra = workbook.createSheet("DetallesDeOrdenesDeCompra");
		Set<OrdenDeCompra> ordenesDeCompra = cargarOrdenesDeCompra(celulares, clientes);
		System.out.println("Creating excel");

		cargarDatosCelulares(sheetCelulares, celulares);
		cargarDatosClientes(sheetClientes, clientes);
		cargarDatosOrdenesDeCompra(sheetOrdenesDeCompra, ordenesDeCompra, sheetDetallesDeOrdenesDeCompra);

		try {
			FileOutputStream outputStream = new FileOutputStream("/tiendaDeTelefonos.xlsx");
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

		// Ahora leo el excel y lo paso a base de datos mySQL

		FileInputStream tiendaCelularesExcel = new FileInputStream(
				new File("/tiendaDeTelefonos.xlsx"));
		Workbook workbooktiendaCelularesExcel = new XSSFWorkbook(tiendaCelularesExcel);
		Sheet celularesSheet = workbooktiendaCelularesExcel.getSheetAt(0);
		Set<Celular> celularesExcel =guardarCelularesEnBaseDeDatos(celularesSheet);
		Sheet clientesSheet = workbooktiendaCelularesExcel.getSheetAt(1);
		Set<Cliente> clientesExcel =guardarclientesEnBaseDeDatos(clientesSheet);
		Sheet ordenesDeCompraSheet = workbooktiendaCelularesExcel.getSheetAt(2);
		Set<OrdenDeCompra> ordenesDeCompraExcel =guardarordenesDeCompraEnBaseDeDatos(ordenesDeCompraSheet,clientesExcel);
		Sheet detallesDeOrdenesDeCompraSheet = workbooktiendaCelularesExcel.getSheetAt(3);
		guardardetallesDeOrdenesEnBaseDeDatos(detallesDeOrdenesDeCompraSheet,celularesExcel,ordenesDeCompraExcel);

	}

	private void guardardetallesDeOrdenesEnBaseDeDatos(Sheet detallesDeOrdenesDeCompraSheet,
			Set<Celular> celularesExcel, Set<OrdenDeCompra> ordenesDeCompraExcel) {
		Iterator<Row> listaDetallesDeOrdenesDeCompra = detallesDeOrdenesDeCompraSheet.iterator();
		listaDetallesDeOrdenesDeCompra.next();
		Set<DetallesDeOrdenDeCompra> detallesordenesDeCompras = new HashSet<>();
		while (listaDetallesDeOrdenesDeCompra.hasNext()) {

			Row currentRow = listaDetallesDeOrdenesDeCompra.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			while (cellIterator.hasNext()) {
				DetallesDeOrdenDeCompra detallesDeOrdenDeCompra= new DetallesDeOrdenDeCompra();
				detallesDeOrdenDeCompra.setCodigo((long)cellIterator.next().getNumericCellValue());
				long idTelefonoABuscar=(long)cellIterator.next().getNumericCellValue();
				Optional<Celular> celular =celularesExcel.stream().filter(cel -> cel.getCodigo() == idTelefonoABuscar).findAny();
				if(celular.isPresent()) {
					detallesDeOrdenDeCompra.setCelular(celular.get());}
				else {
					Celular nuevoCelular=new Celular(idTelefonoABuscar);
					celularRepository.save(nuevoCelular);
					detallesDeOrdenDeCompra.setCelular(nuevoCelular);
				}
				long idOrdenDeCompraABuscar=(long)cellIterator.next().getNumericCellValue();
				Optional<OrdenDeCompra> ordenDeCompra =ordenesDeCompraExcel.stream().filter(cel -> cel.getCodigo() == idOrdenDeCompraABuscar).findAny();
				if(ordenDeCompra.isPresent()) {
					detallesDeOrdenDeCompra.setOrdenDeCompra(ordenDeCompra.get());}
				else {
					OrdenDeCompra nuevaOrdenDeCompra=new OrdenDeCompra(idOrdenDeCompraABuscar);
					ordenDeCompraRepository.save(nuevaOrdenDeCompra);
					detallesDeOrdenDeCompra.setOrdenDeCompra(nuevaOrdenDeCompra);
				}
				detallesordenesDeCompras.add(detallesDeOrdenDeCompra);

			}
		}

		detallesDeOrdenDeCompraRepository.saveAll(detallesordenesDeCompras);
	}

	private Set<OrdenDeCompra> guardarordenesDeCompraEnBaseDeDatos(Sheet ordenesDeCompraSheet, Set<Cliente> clientesExcel) {
		Iterator<Row> listaordenesDeCompra = ordenesDeCompraSheet.iterator();
		listaordenesDeCompra.next();
		Set<OrdenDeCompra> ordenesDeCompra = new HashSet<>();
		while (listaordenesDeCompra.hasNext()) {

			Row currentRow = listaordenesDeCompra.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			while (cellIterator.hasNext()) {
				OrdenDeCompra ordenDeCompra= new OrdenDeCompra();
				ordenDeCompra.setCodigo((long)cellIterator.next().getNumericCellValue());
				ordenDeCompra.setFechaString(cellIterator.next().getStringCellValue());
				int dniClienteABuscar=(int)cellIterator.next().getNumericCellValue();
				Optional<Cliente> cliente =clientesExcel.stream().filter(cli -> cli.getDni() == dniClienteABuscar).findAny();
				if(cliente.isPresent()) {
					ordenDeCompra.setCliente(cliente.get());}
				else {
					Cliente nuevoCliente=new Cliente(dniClienteABuscar);
					clienteRepository.save(nuevoCliente);
					ordenDeCompra.setCliente(nuevoCliente);
				}
				ordenDeCompra.setPrecioTotal(cellIterator.next().getNumericCellValue());
				ordenesDeCompra.add(ordenDeCompra);

			}
		}

		ordenDeCompraRepository.saveAll(ordenesDeCompra);
		return ordenesDeCompra;
	}

	private Set<Cliente>  guardarclientesEnBaseDeDatos(Sheet clientesSheet) {
		Iterator<Row> listaclientes = clientesSheet.iterator();
		listaclientes.next();
		Set<Cliente> clientes = new HashSet<>();
		while (listaclientes.hasNext()) {

			Row currentRow = listaclientes.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			while (cellIterator.hasNext()) {
				clientes.add(new Cliente((int)cellIterator.next().getNumericCellValue(),
						cellIterator.next().getStringCellValue(), cellIterator.next().getStringCellValue(),
						cellIterator.next().getStringCellValue(), cellIterator.next().getStringCellValue()));

			}
		}
		clienteRepository.saveAll(clientes);
		return clientes;
	}

	private Set<Celular> guardarCelularesEnBaseDeDatos(Sheet celularesSheet) {
		Iterator<Row> listaCelulares = celularesSheet.iterator();
		listaCelulares.next();
		Set<Celular> celulares = new HashSet<>();
		while (listaCelulares.hasNext()) {

			Row currentRow = listaCelulares.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			while (cellIterator.hasNext()) {
				celulares.add(new Celular((long) cellIterator.next().getNumericCellValue(),
						cellIterator.next().getStringCellValue(), cellIterator.next().getStringCellValue(),
						cellIterator.next().getNumericCellValue(), cellIterator.next().getBooleanCellValue()));

			}
		}

		celularRepository.saveAll(celulares);
		return celulares;
	}

	private void cargarDatosCelulares(XSSFSheet sheetCelulares, Set<Celular> celulares) {
		int rowNum = 0;
		Row row = sheetCelulares.createRow(rowNum++);
		int colNum = 0;
		Cell cell = row.createCell(colNum++);
		cell.setCellValue("CODIGO");
		cell = row.createCell(colNum++);
		cell.setCellValue("MARCA");
		cell = row.createCell(colNum++);
		cell.setCellValue("MODELO");
		cell = row.createCell(colNum++);
		cell.setCellValue("PRECIO");
		cell = row.createCell(colNum++);
		cell.setCellValue("EN STOCK");
		for (Celular celular : celulares) {
			colNum = 0;
			row = sheetCelulares.createRow(rowNum++);
			cell = row.createCell(colNum++);
			cell.setCellValue(celular.getCodigo());
			cell = row.createCell(colNum++);
			cell.setCellValue(celular.getMarca());
			cell = row.createCell(colNum++);
			cell.setCellValue(celular.getModelo());
			cell = row.createCell(colNum++);
			cell.setCellValue((double) celular.getPrecio());
			cell = row.createCell(colNum++);
			cell.setCellValue((boolean) celular.getDisponible());
		}
	}

	private void cargarDatosClientes(XSSFSheet sheetClientes, Set<Cliente> clientes) {
		int rowNum = 0;
		Row row = sheetClientes.createRow(rowNum++);
		int colNum = 0;
		Cell cell = row.createCell(colNum++);
		cell.setCellValue("DNI");
		cell = row.createCell(colNum++);
		cell.setCellValue("NOMBRE");
		cell = row.createCell(colNum++);
		cell.setCellValue("APELLIDO");
		cell = row.createCell(colNum++);
		cell.setCellValue("DIRECCION");
		cell = row.createCell(colNum++);
		cell.setCellValue("TELEFONO");
		for (Cliente cliente : clientes) {
			colNum = 0;
			row = sheetClientes.createRow(rowNum++);
			cell = row.createCell(colNum++);
			cell.setCellValue(cliente.getDni());
			cell = row.createCell(colNum++);
			cell.setCellValue(cliente.getNombre());
			cell = row.createCell(colNum++);
			cell.setCellValue(cliente.getApellido());
			cell = row.createCell(colNum++);
			cell.setCellValue(cliente.getDireccion());
			cell = row.createCell(colNum++);
			cell.setCellValue(cliente.getTelefono());
		}
	}

	private void cargarDatosOrdenesDeCompra(XSSFSheet sheetOrdenesDeCompra, Set<OrdenDeCompra> ordenesDeCompra,
			XSSFSheet sheetDetallesOrdenesDeCompra) {
		int rowNumOrdenesDeCompra = 0;
		int colNumOrdenesDeCompra = 0;
		Row rowOrdenesDeCompra = sheetOrdenesDeCompra.createRow(rowNumOrdenesDeCompra++);
		int rowNumDetallesOrdenesDeCompra = 0;
		Row rowDetallesOrdenesDeCompra = sheetDetallesOrdenesDeCompra.createRow(rowNumDetallesOrdenesDeCompra++);
		int colNumDetallesOrdenesDeCompra = 0;

		Cell cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
		cellOrdenesDeCompra.setCellValue("CODIGO");
		cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
		cellOrdenesDeCompra.setCellValue("FECHA");
		cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
		cellOrdenesDeCompra.setCellValue("DNI");
		cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
		cellOrdenesDeCompra.setCellValue("PRECIO_TOTAL");

		Cell cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
		cellDetallesOrdenesDeCompra.setCellValue("CODIGO");
		cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
		cellDetallesOrdenesDeCompra.setCellValue("CELULAR");
		cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
		cellDetallesOrdenesDeCompra.setCellValue("ORDEN_DE_COMPRA");

		for (OrdenDeCompra ordenDeCompra : ordenesDeCompra) {
			colNumOrdenesDeCompra = 0;
			rowOrdenesDeCompra = sheetOrdenesDeCompra.createRow(rowNumOrdenesDeCompra++);
			cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
			cellOrdenesDeCompra.setCellValue(ordenDeCompra.getCodigo());
			cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
			cellOrdenesDeCompra.setCellValue(ordenDeCompra.getFecha().toString());
			cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
			cellOrdenesDeCompra.setCellValue(ordenDeCompra.getCliente().getDni());
			cellOrdenesDeCompra = rowOrdenesDeCompra.createCell(colNumOrdenesDeCompra++);
			cellOrdenesDeCompra.setCellValue((double) ordenDeCompra.getPrecioTotal());
			for (DetallesDeOrdenDeCompra detallesDeOrdenDeCompra : ordenDeCompra.getDetallesDeOrdenDeCompra()) {
				colNumDetallesOrdenesDeCompra = 0;
				rowDetallesOrdenesDeCompra = sheetDetallesOrdenesDeCompra.createRow(rowNumDetallesOrdenesDeCompra++);
				cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
				cellDetallesOrdenesDeCompra.setCellValue(detallesDeOrdenDeCompra.getCodigo());
				cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
				cellDetallesOrdenesDeCompra.setCellValue(detallesDeOrdenDeCompra.getCelular().getCodigo());
				cellDetallesOrdenesDeCompra = rowDetallesOrdenesDeCompra.createCell(colNumDetallesOrdenesDeCompra++);
				cellDetallesOrdenesDeCompra.setCellValue(detallesDeOrdenDeCompra.getOrdenDeCompra().getCodigo());
			}
		}
	}

	private Set<Celular> cargarCelulares(int cantidadCelulares) {
		Set<Celular> celulares = new HashSet<>();
		for (int x = 0; x < cantidadCelulares; x++) {
			celulares.add(nuevoCelular());
		}
		return celulares;
	}

	private Set<Cliente> cargarClientes(int cantidadCliente) {
		Set<Cliente> clientes = new HashSet<>();
		for (int x = 0; x < cantidadCliente; x++) {
			clientes.add(nuevoCliente());
		}
		return clientes;
	}

	private Set<OrdenDeCompra> cargarOrdenesDeCompra(Set<Celular> celulares, Set<Cliente> clientes) {
		Set<OrdenDeCompra> ordenesDeCompra = new HashSet<>();
		// Suponiendo que abrio el 1-1-2021
		randomService.setRandomDate(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 9, 28));
		// suponiendo que cada cliente compro entre 1 a 4 celulares
		clientes.forEach(
				c -> ordenesDeCompra.add(comprasRealizadas(c, celulares, randomService.enteroAleatorio(4, 1))));

		return ordenesDeCompra;
	}

	private Celular nuevoCelular() {
		Random r = new Random();
		int nuevoCelular = r.nextInt(7) + 1;
		switch (nuevoCelular) {
		case 1:
			return new Iphone8();
		case 2:
			return new Iphone11();
		case 3:
			return new GS();
		case 4:
			return new GA();
		case 5:
			return new G6();
		case 6:
			return new G7();
		case 7:
			return new G8();
		default:
			break;
		}
		return null;
	}

	private Cliente nuevoCliente() {
		return new Cliente(randomService.enteroAleatorio(40000000, 20000000),
				randomService.createRandomStringWhitFirthLetterUpperAndInLength(5),
				randomService.createRandomStringWhitFirthLetterUpperAndInLength(8),
				randomService.createRandomStringWhitFirthLetterUpperAndInLength(15),
				randomService.enteroAleatorio(1199999999, 1100000000));
	}

	private OrdenDeCompra comprasRealizadas(Cliente c, Set<Celular> celulares, int cantidadDeCompras) {
		OrdenDeCompra ordenDeCompra = new OrdenDeCompra(randomService.nextDateTimeOrdenFacutra(), c);
		while (cantidadDeCompras > 0) {
			cantidadDeCompras--;
			Optional<Celular> celularOp = celulares.stream().filter(ce -> ce.getDisponible() == true).findAny();
			if (celularOp.isPresent()) {
				Celular celular = celularOp.get();
				celulares.remove(celular); // Safe even if the element is not in the Set
				celular.setDisponible(false);
				celulares.add(celular);
				ordenDeCompra.addDetalleDeCompra(new DetallesDeOrdenDeCompra(celular, ordenDeCompra));
			} else {
				System.out.println("No hay mas celulares");
			}
		}
		return ordenDeCompra;
	}
}