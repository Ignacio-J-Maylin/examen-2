package com.examen2.demo.component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;

@Service
public class RandomService {

	private static final String lowercaseletters = "abcdefghijklmnopqrstuvwxyz";
	private static final String uppercaseletters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static LocalDate minDate;
	private static LocalDate maxDate;
	private static Random random;

	public String createRandomStringWhitFirthLetterUpperAndInLength(int length) {

		StringBuilder builder;
		// create the StringBuffer
		builder = new StringBuilder(length);
		for (int m = 0; m < length; m++) {
			// generate numeric
			int myindex = (int) (lowercaseletters.length() * Math.random());
			if (m == 0) {
				// add the characters
				builder.append(uppercaseletters.charAt(myindex));
			} else {
				// add the characters
				builder.append(lowercaseletters.charAt(myindex));
			}
		}
		return builder.toString();
	}

	public void setRandomDate(LocalDate minDate, LocalDate maxDate) {
		RandomService.minDate = minDate;
		RandomService.maxDate = maxDate;
		RandomService.random = new Random();
	}

	public LocalDate nextDate() {
		int minDay = (int) minDate.toEpochDay();
		int maxDay = (int) maxDate.toEpochDay();
		long randomDay = minDay + random.nextInt(maxDay - minDay);
		return LocalDate.ofEpochDay(randomDay);
	}

	public LocalDateTime nextDateTimeOrdenFacutra() {
		long minDay = (int) minDate.toEpochDay();
		long maxDay = (int) maxDate.toEpochDay();
		long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
		// hora habil de 8 hs a 18 hs de la tienda
		LocalTime horaHabilDeTrabajo = LocalTime.of((int) ((Math.random() * (18 - 8)) + 8),
				(int) ((Math.random() * (60 - 0)) + 0), (int) ((Math.random() * (60 - 0)) + 0));
		LocalDate randomDate = LocalDate.ofEpochDay(randomDay);
		return LocalDateTime.of(randomDate, horaHabilDeTrabajo);
	}

	public int enteroAleatorio(int max, int min) {
		return (int) ((Math.random() * (max - min)) + min);
	}

	@Override
	public String toString() {
		return "RandomDate{" + "maxDate=" + maxDate + ", minDate=" + minDate + '}';
	}
}
