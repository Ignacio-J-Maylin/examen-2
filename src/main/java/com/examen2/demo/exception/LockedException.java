package com.examen2.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class LockedException extends Exception{

    private static final long serialVersionUID = 1L;

    public LockedException(String message){
        super(message);
    }
}