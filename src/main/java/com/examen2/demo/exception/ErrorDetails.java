package com.examen2.demo.exception;


import java.util.Date;

import org.springframework.http.HttpStatus;


public class ErrorDetails {
	  private int statusCode;
	  private String statusMeaning;
	  private Date timestamp;
	  private String message;
	  private String description;

	  public ErrorDetails(HttpStatus expectationFailed, Date timestamp, String message, String description) {
		    this.statusCode = expectationFailed.value();
		    this.statusMeaning = expectationFailed.name();
		    this.timestamp = timestamp;
		    this.message = message;
		    this.description = description;
	}

	public int getStatusCode() {
	    return statusCode;
	  }

	  public Date getTimestamp() {
	    return timestamp;
	  }

	  public String getMessage() {
	    return message;
	  }

	  public String getDescription() {
	    return description;
	  }

	public String getStatusMeaning() {
		return statusMeaning;
	}

	public void setStatusMeaning(String statusMeaning) {
		this.statusMeaning = statusMeaning;
	}
}