package com.examen2.demo.service;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import com.examen2.demo.exception.PreConditionFailedException;
import com.examen2.demo.exception.ResourceNotFoundException;
import com.examen2.demo.model.dto.OrdenDeCompraDTO;
import com.examen2.demo.request.OrdenDeCompraRequest;

public interface ApiService {

	OrdenDeCompraDTO crearOrdenDeCompra(@Valid OrdenDeCompraRequest ordenDeCompraRequest) throws ResourceNotFoundException, PreConditionFailedException;

	Page<OrdenDeCompraDTO> traerUltimas30OrdenesDeCompraDTO(int pageNumber);

	OrdenDeCompraDTO findOrdenDeCompraById(Long id) throws ResourceNotFoundException;

	OrdenDeCompraRequest getOrdenDeCompraRequestRandom();

}
