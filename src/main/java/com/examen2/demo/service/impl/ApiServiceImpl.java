package com.examen2.demo.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.examen2.demo.exception.PreConditionFailedException;
import com.examen2.demo.exception.ResourceNotFoundException;
import com.examen2.demo.model.dto.OrdenDeCompraDTO;
import com.examen2.demo.model.entity.Celular;
import com.examen2.demo.model.entity.Cliente;
import com.examen2.demo.model.entity.DetallesDeOrdenDeCompra;
import com.examen2.demo.model.entity.OrdenDeCompra;
import com.examen2.demo.repository.CelularRepository;
import com.examen2.demo.repository.ClienteRepository;
import com.examen2.demo.repository.OrdenDeCompraRepository;
import com.examen2.demo.request.OrdenDeCompraRequest;
import com.examen2.demo.service.ApiService;

@Service
public class ApiServiceImpl implements ApiService {

	@Autowired
	ClienteRepository clienteRepository;
	@Autowired
	CelularRepository celularRepository;
	@Autowired
	OrdenDeCompraRepository ordenDeCompraRepository;

	@Override
	public OrdenDeCompraDTO crearOrdenDeCompra(@Valid OrdenDeCompraRequest ordenDeCompraRequest) throws ResourceNotFoundException, PreConditionFailedException {

		List<Celular> listaCompra = celularRepository.findAllByCodigoAndDisponibleTrue(ordenDeCompraRequest.getListaCompra());
		if(listaCompra.size()==0) {
			throw new PreConditionFailedException("Ningun codigo de celular estaba habilitado");
		}
		Cliente cliente = clienteRepository.findById(ordenDeCompraRequest.getDni()).orElse(new Cliente(ordenDeCompraRequest.getDni()));
		StringBuffer detalles= new StringBuffer("");
		if(cliente.getDireccion().isBlank()) {
			Cliente clienteBD = clienteRepository.save(cliente);
			detalles.append("Se creo un nuevo cliente con ese DNI y sus datos vacios. Por favor carga sus datos en la siguiente uri"
					+ "put /api/cliente/"+clienteBD.getDni()+ "  pasandole por el body el nombre, apellido, direccion y telefono. ");
		}
		List<DetallesDeOrdenDeCompra> detallesDeOrdenDeCompra = new ArrayList<>();
		OrdenDeCompra ordenDeCompra =new OrdenDeCompra(cliente);
		listaCompra.forEach(c->{
			detallesDeOrdenDeCompra.add(new DetallesDeOrdenDeCompra(c,ordenDeCompra));
			c.setDisponible(false);
		});
		celularRepository.saveAll(listaCompra);
		ordenDeCompra.addDetalleDeCompra(detallesDeOrdenDeCompra);
		OrdenDeCompra ordenDeCompraBD =ordenDeCompraRepository.save(ordenDeCompra);
		detalles.append("Lista de compras :");
		listaCompra.forEach(c->detalles.append("Codigo: "+c.getCodigo().toString()+", marca: "+c.getMarca()+" ,modelo: " +c.getModelo()+", precio:"+c.getPrecio().toString()+". "));
		if(listaCompra.size()!=ordenDeCompraRequest.getListaCompra().size())
		{
			listaCompra.forEach(c->ordenDeCompraRequest.getListaCompra().remove(c.getCodigo()));
			detalles.append("No se pudieron cargar todos los celulares.Codigos celular"
					+ "que no se encontraron : ");
			ordenDeCompraRequest.getListaCompra().forEach(id->detalles.append(" | "+id.toString()+"  "));
			detalles.append( "| .Si queres agregar los celulares, podes probar poniendo "
					+ " otro codigo mandando la modificacion al siguiente uri post /api/ordenDeCompra/"+ordenDeCompraBD.getCodigo()+"/detallesDeOrdenDeCompra pasandole por body el nuevo codigo."
							+ "Si es correcto se va agregar , se actualiza la fecha de orden de compra y tiene que ser una orden de compra que no esta cerrada.");
			
		}
		return new OrdenDeCompraDTO(ordenDeCompraBD,detalles.toString());
	}

	@Override
	public Page<OrdenDeCompraDTO> traerUltimas30OrdenesDeCompraDTO(int pageNumber) {
		Pageable pageOf30Items = PageRequest.of(pageNumber, 30);
		Page<OrdenDeCompraDTO>  ordenDeCompraDTOList = ordenDeCompraRepository.findAllOrderByFechaDesc(pageOf30Items);
		return ordenDeCompraDTOList;
	}

	@Override
	public OrdenDeCompraDTO findOrdenDeCompraById(Long id) throws ResourceNotFoundException {
		OrdenDeCompraDTO ordenDeCompraDTO =ordenDeCompraRepository.findByCodigo(id)
				.orElseThrow(() -> new ResourceNotFoundException("ordenDeCompraDTO no encontrada"
				+ " con el codigo : " + id));
		return ordenDeCompraDTO;
	}

	@Override
	public OrdenDeCompraRequest getOrdenDeCompraRequestRandom() {
		List<Celular> celularesDisponibles = celularRepository.findFirst3ByDisponibleTrue();
    	List<Celular> celularesNoDisponibles = celularRepository.findFirst3ByDisponibleFalse();

		Cliente cliente  = clienteRepository.findFirstByDniNotNull();

    	OrdenDeCompraRequest ordenDeCompraRequest = new OrdenDeCompraRequest();
    	ordenDeCompraRequest.setDni(cliente.getDni());
    	Set<Long> celularesCodigos = new HashSet<>();
    	celularesDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
    	celularesNoDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
    	ordenDeCompraRequest.setListaCompra(celularesCodigos);
    	return ordenDeCompraRequest;
	}

}
