package com.examen2.demo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen2.demo.exception.PreConditionFailedException;
import com.examen2.demo.exception.ResourceNotFoundException;
import com.examen2.demo.model.dto.OrdenDeCompraDTO;
import com.examen2.demo.request.OrdenDeCompraRequest;
import com.examen2.demo.service.ApiService;




@RestController
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private ApiService apiSerice;

	// 3.2 Servicio que me cree una orden de compra.
	@PostMapping("/ordenDeCompra")
	public ResponseEntity<OrdenDeCompraDTO> crearOrdenDeCompra(@Valid @RequestBody OrdenDeCompraRequest ordenDeCompraRequest) throws ResourceNotFoundException, PreConditionFailedException  {
		OrdenDeCompraDTO response = apiSerice.crearOrdenDeCompra(ordenDeCompraRequest);
		URI uri =URI.create("/api/ordenDeCompra/"+response.getCodigo());
		return ResponseEntity.created(uri).body(response);
	}
	
	
	// 3.3 Generar un servicio que me retorne la lista de órdenes de compra.
	@GetMapping("/ordenesDeCompra")
	public ResponseEntity<Page<OrdenDeCompraDTO> > traerUltimas30OrdenesDeCompraDTO()
			throws ResourceNotFoundException {
		Page<OrdenDeCompraDTO> last10GeneralInvoice = apiSerice.traerUltimas30OrdenesDeCompraDTO(0);
		return ResponseEntity.ok().body(last10GeneralInvoice);
	}
	
	// 3.4 Generar un servicio que me retorne una orden de compra usando su número de orden.
	@GetMapping("/ordenDeCompra/{id}")
	public ResponseEntity<OrdenDeCompraDTO> getordenDeCompra(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException {
		OrdenDeCompraDTO ordenDeCompraDTO = apiSerice.findOrdenDeCompraById(id);
		return ResponseEntity.ok().body(ordenDeCompraDTO);
	}

//	• Uso de Streams y Lambdas
//	• Generar el resto de los servicios CRUD para Productos y clientes.
//	• Test unitarios.
	
	@GetMapping("/ordenDeCompraRequest/random")
	public ResponseEntity<OrdenDeCompraRequest> getOrdenDeCompraRequestRandom()
			throws ResourceNotFoundException {
		OrdenDeCompraRequest ordenDeCompraRequest = apiSerice.getOrdenDeCompraRequestRandom();
		return ResponseEntity.ok().body(ordenDeCompraRequest);
	}
	
}
