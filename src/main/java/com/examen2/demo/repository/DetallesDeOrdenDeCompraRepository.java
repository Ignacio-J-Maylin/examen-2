package com.examen2.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examen2.demo.model.entity.DetallesDeOrdenDeCompra;


@Repository
public interface DetallesDeOrdenDeCompraRepository extends JpaRepository<DetallesDeOrdenDeCompra,Long> {

}
