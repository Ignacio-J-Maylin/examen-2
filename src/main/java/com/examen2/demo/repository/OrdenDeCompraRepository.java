package com.examen2.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen2.demo.model.dto.OrdenDeCompraDTO;
import com.examen2.demo.model.entity.OrdenDeCompra;


@Repository
public interface OrdenDeCompraRepository extends JpaRepository<OrdenDeCompra,Long> {

	@Query(value ="SELECT  o from OrdenDeCompra o join fetch o.cliente join fetch o.detallesDeOrdenDeCompra order by o.codigo ASC",
			countQuery = "SELECT count(*) from OrdenDeCompra o "	)
	Page<OrdenDeCompraDTO> findAllOrderByFechaDesc(Pageable pageOf30Items);

	@Query("SELECT o from OrdenDeCompra o join fetch o.cliente join fetch o.detallesDeOrdenDeCompra  where o.codigo = :codigo")
	Optional<OrdenDeCompraDTO>  findByCodigo(Long codigo);

	OrdenDeCompra findFirstByCodigoNotNull();
	

}
