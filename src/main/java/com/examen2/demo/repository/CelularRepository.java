package com.examen2.demo.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen2.demo.model.entity.Celular;


@Repository
public interface CelularRepository extends JpaRepository<Celular, Long> {

	@Query("SELECT c from Celular c where c.codigo in (:listaCompra) and c.disponible =1")
	List<Celular> findAllByCodigoAndDisponibleTrue(Set<Long> listaCompra);

	Optional<Celular> findFirstByDisponibleTrue();


	List<Celular> findFirst3ByDisponibleTrue();

	List<Celular> findFirst3ByDisponibleFalse();

}
