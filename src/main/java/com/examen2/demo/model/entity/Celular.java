package com.examen2.demo.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "CELULAR")
public class Celular {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="CODIGO", nullable=false, unique=true)
	private Long codigo;
	
	@Column(name = "MARCA")
	private String marca;

	@Column(name = "MODELO")
	private String modelo;

	@Column(name = "PRECIO")
	private Double precio;
	
	@Column(name = "DISPONIBLE")
	private Boolean disponible;

	private static int ultimoId=1;
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Boolean getDisponible() {
		return disponible;
	}

	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}

	public Celular(String marca, String modelo, Double precio) {
		this.codigo=(long)ultimoId++;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.disponible = true;
	}

	public Celular() {
		// TODO Auto-generated constructor stub
	}

	public Celular(long numericCellValue, String stringCellValue, String stringCellValue2, double numericCellValue2,
			boolean booleanCellValue) {
		this.codigo=numericCellValue;
		this.marca = stringCellValue;
		this.modelo = stringCellValue2;
		this.precio = numericCellValue2;
		this.disponible = booleanCellValue;
	}

	public Celular(long idTelefonoABuscar) {
		this.codigo=idTelefonoABuscar;
		this.marca = "";
		this.modelo = "";
		this.precio = 0.0;
		this.disponible = false;
	}

	@Override
	public String toString() {
		return "Celular [codigo=" + codigo + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", disponible=" + disponible + "]";
	}

	
	
}