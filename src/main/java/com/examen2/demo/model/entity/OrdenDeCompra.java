package com.examen2.demo.model.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ORDEN_DE_COMPRA")
public class OrdenDeCompra {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CODIGO", nullable = false, unique = true)
	private Long codigo;

	@Column(name = "FECHA")
	private LocalDateTime fecha;

	//@ManyToOne(fetch = FetchType.LAZY) para correr los test el cascade fue para crear las tablas desde el excel
	@ManyToOne(fetch = FetchType.LAZY,cascade = { CascadeType.ALL })
	@JoinColumn(name = "DNI")
	private Cliente cliente;

	@Column(name = "PRECIO_TOTAL")
	private Double precioTotal;

	@OneToMany(mappedBy = "ordenDeCompra", fetch = FetchType.LAZY)
	private List<DetallesDeOrdenDeCompra> detallesDeOrdenDeCompra = new ArrayList<DetallesDeOrdenDeCompra>();

	private static int ultimoId = 1;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(Double precioTotal) {
		this.precioTotal = precioTotal;
	}

	public List<DetallesDeOrdenDeCompra> getDetallesDeOrdenDeCompra() {
		return detallesDeOrdenDeCompra;
	}

	public void setDetallesDeOrdenDeCompra(List<DetallesDeOrdenDeCompra> detallesDeOrdenDeCompra) {
		this.detallesDeOrdenDeCompra = detallesDeOrdenDeCompra;
	}

	public OrdenDeCompra() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrdenDeCompra(LocalDateTime fecha, Cliente cliente) {
		this.codigo = (long) ultimoId++;
		this.fecha = fecha;
		this.cliente = cliente;
		this.precioTotal = (double) 0;
	}

	public OrdenDeCompra(long idOrdenDeCompraABuscar) {
		this.codigo = idOrdenDeCompraABuscar;
		this.fecha = null;
		this.cliente = null;
		this.precioTotal = (double) 0;
	}

	public OrdenDeCompra(Cliente cliente) {
		this.fecha = LocalDateTime.now();
	    this.cliente = cliente;
	    this.precioTotal = (double) 0;
	}

	public void addDetalleDeCompra(DetallesDeOrdenDeCompra detallesDeOrdenDeCompra) {
		this.detallesDeOrdenDeCompra.add(detallesDeOrdenDeCompra);
		this.precioTotal = (this.precioTotal + detallesDeOrdenDeCompra.getCelular().getPrecio());

	}

	public void setFechaString(String stringCellValue) {
		this.fecha = LocalDateTime.of(Integer.valueOf(stringCellValue.substring(0, 4)),
				Integer.valueOf(stringCellValue.substring(5, 7)), Integer.valueOf(stringCellValue.substring(8, 10)),
				Integer.valueOf(stringCellValue.substring(11, 13)), Integer.valueOf(stringCellValue.substring(14, 16)),
				00);

	}

	@Override
	public String toString() {
		return "OrdenDeCompra [codigo=" + codigo + ", fecha=" + fecha + ", cliente=" + cliente + ", precioTotal="
				+ precioTotal + ", detallesDeOrdenDeCompra=" + detallesDeOrdenDeCompra + "]";
	}

	public void addDetalleDeCompra(List<DetallesDeOrdenDeCompra> detallesDeOrdenDeCompra) {
		detallesDeOrdenDeCompra.forEach(d->addDetalleDeCompra(d));
		
	}

}
