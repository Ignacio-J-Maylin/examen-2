package com.examen2.demo.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DETALLES_DE_ORDEN_DE_COMPRA")
public class DetallesDeOrdenDeCompra {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CODIGO", nullable=false, unique=true)
	private Long codigo;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "CELULAR")
	private Celular celular;
	@ManyToOne(fetch = FetchType.LAZY,cascade = { CascadeType.ALL })
	@JoinColumn(name = "ORDEN_DE_COMPRA")
	private OrdenDeCompra ordenDeCompra;
	

	private static int ultimoId=1;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public Celular getCelular() {
		return celular;
	}
	public void setCelular(Celular celular) {
		this.celular = celular;
	}
	public OrdenDeCompra getOrdenDeCompra() {
		return ordenDeCompra;
	}
	public void setOrdenDeCompra(OrdenDeCompra ordenDeCompra) {
		this.ordenDeCompra = ordenDeCompra;
	}
	public DetallesDeOrdenDeCompra() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DetallesDeOrdenDeCompra( Celular celular, OrdenDeCompra ordenDeCompra) {
		super();
		this.codigo=(long)ultimoId++;
		this.celular = celular;
		this.ordenDeCompra = ordenDeCompra;
	}
	
	
	
}
