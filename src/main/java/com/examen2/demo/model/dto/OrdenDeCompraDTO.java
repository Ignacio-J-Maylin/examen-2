package com.examen2.demo.model.dto;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

import com.examen2.demo.model.entity.OrdenDeCompra;

public class OrdenDeCompraDTO {

	private Long codigo;
	private String fecha;
	private String nombreYApellidoCliente;
	private Double precioTotal ;
	private String descripcionDeLaCompra ;
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	public OrdenDeCompraDTO(OrdenDeCompra ordenDeCompra, String descripcionDeLaCompra) {super();
	this.codigo = ordenDeCompra.getCodigo();
	this.fecha = ordenDeCompra.getFecha().format(formatter);
	this.nombreYApellidoCliente = ordenDeCompra.getCliente().getNombre()+", "+ordenDeCompra.getCliente().getApellido();
	this.precioTotal = ordenDeCompra.getPrecioTotal();
	this.descripcionDeLaCompra =descripcionDeLaCompra;
	}
	public OrdenDeCompraDTO(OrdenDeCompra ordenDeCompra) {
		this.codigo = ordenDeCompra.getCodigo();
	this.fecha = ordenDeCompra.getFecha().format(formatter);
	this.nombreYApellidoCliente = ordenDeCompra.getCliente().getNombre()+", "+ordenDeCompra.getCliente().getApellido();
	this.precioTotal = ordenDeCompra.getPrecioTotal();
	StringBuffer detalles= new StringBuffer("");
	detalles.append("Lista de compras : ");
	ordenDeCompra.getDetallesDeOrdenDeCompra().forEach(
			c->detalles.append("Codigo: "+c.getCelular().getCodigo().toString()+
					", marca: "+c.getCelular().getMarca()+" ,modelo: " +c.getCelular().getModelo()+
					", precio:"+c.getCelular().getPrecio().toString()+". "));
	this.descripcionDeLaCompra =detalles.toString();
	}
	public OrdenDeCompraDTO(OrdenDeCompraDTO ordenDeCompra) {
		super();
		this.codigo = ordenDeCompra.getCodigo();
		this.fecha = ordenDeCompra.getFecha();
		this.nombreYApellidoCliente = ordenDeCompra.getNombreYApellidoCliente();
		this.precioTotal = ordenDeCompra.getPrecioTotal();
		this.descripcionDeLaCompra = ordenDeCompra.getDescripcionDeLaCompra();

	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNombreYApellidoCliente() {
		return nombreYApellidoCliente;
	}
	public void setNombreYApellidoCliente(String nombreYApellidoCliente) {
		this.nombreYApellidoCliente = nombreYApellidoCliente;
	}
	public Double getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(Double precioTotal) {
		this.precioTotal = precioTotal;
	}
	public String getDescripcionDeLaCompra() {
		return descripcionDeLaCompra;
	}
	public void setDescripcionDeLaCompra(String descripcionDeLaCompra) {
		this.descripcionDeLaCompra = descripcionDeLaCompra;
	}
	@Override
	public String toString() {
		return "OrdenDeCompraDTO [codigo=" + codigo + ", fecha=" + fecha + ", nombreYApellidoCliente="
				+ nombreYApellidoCliente + ", precioTotal=" + precioTotal + ", descripcionDeLaCompra="
				+ descripcionDeLaCompra + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(codigo, descripcionDeLaCompra, fecha, nombreYApellidoCliente, precioTotal);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenDeCompraDTO other = (OrdenDeCompraDTO) obj;
		return Objects.equals(codigo, other.codigo)
				&& Objects.equals(descripcionDeLaCompra, other.descripcionDeLaCompra)
				&& Objects.equals(fecha, other.fecha)
				&& Objects.equals(nombreYApellidoCliente, other.nombreYApellidoCliente)
				&& Objects.equals(precioTotal, other.precioTotal);
	}
	
	
}
