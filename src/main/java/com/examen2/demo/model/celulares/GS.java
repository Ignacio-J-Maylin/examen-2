package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class GS extends Celular {

	public GS() {
		super("SAMSUNG", "Galaxy S", 1200.00);
	}

}
