package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class G7 extends Celular {

	public G7() {
		super("SAMSUNG", "Galaxy 7", 600.00);
	}

}
