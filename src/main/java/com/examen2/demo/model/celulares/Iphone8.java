package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class Iphone8 extends Celular {

	public Iphone8() {
		super("IPHONE", "8", 600.00);
	}

}
