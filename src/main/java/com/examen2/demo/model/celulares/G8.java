package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class G8 extends Celular {

	public G8() {
		super("SAMSUNG", "Galaxy 8", 700.00);
	}

}
