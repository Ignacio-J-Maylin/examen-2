package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class G6 extends Celular {

	public G6( ) {
		super("SAMSUNG", "Galaxy 6", 400.00);
	}

}
