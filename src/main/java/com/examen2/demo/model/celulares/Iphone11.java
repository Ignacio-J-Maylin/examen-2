package com.examen2.demo.model.celulares;

import com.examen2.demo.model.entity.Celular;

public class Iphone11 extends Celular {

	public Iphone11() {
		super("IPHONE", "11", 1200.00);
	}

}
