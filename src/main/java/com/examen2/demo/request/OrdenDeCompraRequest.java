package com.examen2.demo.request;

import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class OrdenDeCompraRequest {
	

	@NotNull(message = "El DNI es requerido para crear la orden de compra.")
	private Integer dni;
	@Size(min = 1,message = "Tienen que haber productos cargados.")
	private Set<Long> listaCompra;
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	public Set<Long> getListaCompra() {
		return listaCompra;
	}
	public void setListaCompra(Set<Long> listaCompra) {
		this.listaCompra = listaCompra;
	}
	@Override
	public int hashCode() {
		return Objects.hash(dni, listaCompra);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenDeCompraRequest other = (OrdenDeCompraRequest) obj;
		return Objects.equals(dni, other.dni) && Objects.equals(listaCompra, other.listaCompra);
	}

}
