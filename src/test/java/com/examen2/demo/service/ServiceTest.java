package com.examen2.demo.service;
//
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//
//import com.examen2.demo.exception.PreConditionFailedException;
//import com.examen2.demo.exception.ResourceNotFoundException;
//import com.examen2.demo.model.dto.OrdenDeCompraDTO;
//import com.examen2.demo.model.entity.Celular;
//import com.examen2.demo.model.entity.Cliente;
//import com.examen2.demo.model.entity.OrdenDeCompra;
//import com.examen2.demo.repository.CelularRepository;
//import com.examen2.demo.repository.ClienteRepository;
//import com.examen2.demo.repository.OrdenDeCompraRepository;
//import com.examen2.demo.request.OrdenDeCompraRequest;
//
//
//@SpringBootTest
public class ServiceTest {

//	@Autowired
//	private ApiService apiSerice;
//	
//
//	@Autowired
//	private ClienteRepository clienteRepository;
//	@Autowired
//	private CelularRepository celularRepository;
//	@Autowired
//	private OrdenDeCompraRepository ordenDeCompraRepository;
//
//    @Test
//    public void deveriaCrearOrdenDeCompra() throws ResourceNotFoundException, PreConditionFailedException {
//    	List<Celular> celularesDisponibles = celularRepository.findFirst3ByDisponibleTrue();
//    	List<Celular> celularesNoDisponibles = celularRepository.findFirst3ByDisponibleFalse();
//
//		int esperado = 3;
//		assertThat(celularesDisponibles).hasSize(esperado);
//		assertThat(celularesNoDisponibles).hasSize(esperado);
//		Cliente cliente  = clienteRepository.findFirstByDniNotNull();
//
//    	OrdenDeCompraRequest ordenDeCompraRequest = new OrdenDeCompraRequest();
//    	ordenDeCompraRequest.setDni(cliente.getDni());
//    	Set<Long> celularesCodigos = new HashSet<>();
//    	celularesDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
//    	celularesNoDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
//    	ordenDeCompraRequest.setListaCompra(celularesCodigos);
//    	OrdenDeCompraDTO  resultado =  apiSerice.crearOrdenDeCompra(ordenDeCompraRequest);
//		assertThat(resultado).isNotNull();
//		Double precioTotalCelularesDisponibles =  celularesDisponibles.stream()
//				  .mapToDouble(x -> x.getPrecio())
//				  .sum();
//		assertThat(resultado.getPrecioTotal()).isEqualTo(precioTotalCelularesDisponibles);
//		
//    }
//
//    @Test
//    public void deveriaEncontrarOrdenDeCompra() throws ResourceNotFoundException {
//    	OrdenDeCompra ordenDeCompra = ordenDeCompraRepository.findFirstByCodigoNotNull();
//    	OrdenDeCompraDTO  resultado =  apiSerice.findOrdenDeCompraById(ordenDeCompra.getCodigo());
//		assertThat(resultado).isNotNull();
//		assertThat(resultado.getCodigo()).isEqualTo(ordenDeCompra.getCodigo());
//		
//    }
//
//    @Test
//    public void deveriaTraerUltimas30OrdenesDeCompraDTO() throws ResourceNotFoundException {
//    	int pageNumber = 0 ; 
//    	int listNumber = 30 ; 
//    	Page<OrdenDeCompraDTO>  resultado =  apiSerice.traerUltimas30OrdenesDeCompraDTO(pageNumber);
//		assertThat(resultado).isNotNull();
//		assertThat(resultado.getNumber()).isEqualTo(pageNumber);
//		assertThat(resultado.getSize()).isEqualTo(listNumber);
//		
//    }
//

}
