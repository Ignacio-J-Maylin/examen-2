package com.examen2.demo.controller;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import org.junit.Assert;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.examen2.demo.model.entity.Celular;
//import com.examen2.demo.model.entity.Cliente;
//import com.examen2.demo.repository.CelularRepository;
//import com.examen2.demo.repository.ClienteRepository;
//import com.examen2.demo.request.OrdenDeCompraRequest;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@TestPropertySource(locations = "classpath:db-test.properties")
public class ControllerTest {
//
//	@Autowired
//	private TestRestTemplate restTemplate;
//
//	@LocalServerPort
//	int randomServerPort;
//
//	@Autowired
//	private ClienteRepository clienteRepository;
//	@Autowired
//	private CelularRepository celularRepository;
//	
//	@Test
//	public void testCrearOrdenDeCompraSuccess() throws URISyntaxException {
//
//    	List<Celular> celularesDisponibles = celularRepository.findFirst3ByDisponibleTrue();
//    	List<Celular> celularesNoDisponibles = celularRepository.findFirst3ByDisponibleFalse();
//		int esperado = 3;
//		assertThat(celularesDisponibles).hasSize(esperado);
//		assertThat(celularesNoDisponibles).hasSize(esperado);
//		Cliente cliente  = clienteRepository.findFirstByDniNotNull();
//
//    	OrdenDeCompraRequest ordenDeCompraRequest = new OrdenDeCompraRequest();
//    	ordenDeCompraRequest.setDni(cliente.getDni());
//    	Set<Long> celularesCodigos = new HashSet<>();
//    	celularesDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
//    	celularesNoDisponibles.forEach(c->celularesCodigos.add(c.getCodigo()));
//    	ordenDeCompraRequest.setListaCompra(celularesCodigos);
//		
//		final String baseUrl = "http://localhost:" + randomServerPort + "/api/ordenDeCompra";
//		URI uri = new URI(baseUrl);
//		HttpHeaders headers = new HttpHeaders();
//		HttpEntity<OrdenDeCompraRequest> request = new HttpEntity<>(ordenDeCompraRequest, headers);
//		ResponseEntity<String> resultado = this.restTemplate.postForEntity(uri, request, String.class);
//		Assert.assertEquals(201, resultado.getStatusCodeValue());
//	}
//
//	@Test
//	public void testgetordenesDeCompraSuccess() throws URISyntaxException {
//		final String baseUrl = "http://localhost:" + randomServerPort + "/api/ordenesDeCompra";
//		URI uri = new URI(baseUrl);
//		ResponseEntity<String> result = this.restTemplate.getForEntity(uri, String.class);
//		Assert.assertEquals(200, result.getStatusCodeValue());
//	}
//	@Test
//	public void testgetordenDeCompraByCodigoSuccess() throws URISyntaxException {
//		final String baseUrl = "http://localhost:" + randomServerPort + "/api/ordenDeCompra/1";
//		URI uri = new URI(baseUrl);
//		ResponseEntity<String> result = this.restTemplate.getForEntity(uri, String.class);
//		Assert.assertEquals(200, result.getStatusCodeValue());
//	}

}